﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Webone.Logger.Info;
using System.Text;
using System.IO;
using JCwx.Tools.Common.MessageTools;
using JCwx.Weixin.Lib.StdLib;

namespace YaPeiDemo
{
    /// <summary>
    /// Service 的摘要说明
    /// </summary>
    public class Service : IHttpHandler
    {

        string token = ConfigurationManager.AppSettings["Token"].ToString();
        string AppId = ConfigurationManager.AppSettings["AppId"].ToString();
        string EncodingAESKey = ConfigurationManager.AppSettings["EncodingAESKey"].ToString();
        string AppSecret = ConfigurationManager.AppSettings["AppSecret"].ToString();

        string signature = HttpContext.Current.Request.QueryString["signature"];
        string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
        string nonce = HttpContext.Current.Request.QueryString["nonce"];
        string echostr = HttpContext.Current.Request.QueryString["echostr"];

        /// <summary>
        /// 网络入口
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string postString = string.Empty;
            if (HttpContext.Current.Request.HttpMethod.ToUpper() == "GET")
            {
                Auth();
            }
            else if (HttpContext.Current.Request.HttpMethod.ToUpper() == "POST")
            {
                InfoLogger.LogInfo("DoMessage()||处理用户请求");
                DoMessage();//处理用户发过来的信息
            }
        }

        #region 用户消息处理接口
        public void DoMessage()
        {
            try
            {
                //接收网络数据
                Stream sr = HttpContext.Current.Request.InputStream;
                Byte[] data = new Byte[sr.Length];
                sr.Read(data, 0, (Int32)sr.Length);
                string message = Encoding.UTF8.GetString(data);
                InfoLogger.LogInfo("收到的加密消息：" + message);
                HandleMessage handle = new HandleMessage();
                WXBizMsgCrypt wxcpt = new WXBizMsgCrypt(token, EncodingAESKey, AppId);

                string decryptMsg = string.Empty; //解密后的消息，XML格式

                if (wxcpt.DecryptMsg(signature, timestamp, nonce, message, ref  decryptMsg) == 0)
                {
                    if (!string.IsNullOrEmpty(decryptMsg))
                    {
                        InfoLogger.LogInfo("解密后的消息：" + decryptMsg);
                        string returnMsg = handle.Handler(decryptMsg);//对用户发来的消息进行处理并回传要发送给用户的消息，XML格式
                        InfoLogger.LogInfo("回复给用户的消息：" + returnMsg);
                        string encrypt_msg = string.Empty; //保存加密后的returnMessage
                        if (wxcpt.EncryptMsg(returnMsg, timestamp, nonce, ref  encrypt_msg) == 0)
                        {
                            if (!string.IsNullOrEmpty(encrypt_msg))
                            {
                                HttpContext.Current.Response.Write(encrypt_msg);
                                HttpContext.Current.Response.End();

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                InfoLogger.LogInfo("QYService||DoMessage()");
                InfoLogger.LogInfo(ex.Message);
            }
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 验证用户身份
        /// </summary>
        private void Auth()
        {
           

            //get method - 仅在微信后台填写URL验证时触发
            if (CheckSignature(signature, timestamp, nonce, token))
            {
                WriteContent(echostr); //返回随机字符串则表示验证通过
            }
            else
            {
                WriteContent("failed:" + signature + "," + GetSignature(timestamp, nonce, token));
            }
            HttpContext.Current.Response.End();

        }

        /// <summary>
        /// 输出信息到网页
        /// </summary>
        /// <param name="str"></param>
        private void WriteContent(string str)
        {
            HttpContext.Current.Response.Output.Write(str);
        }

        /// <summary>
        /// 本地签名与微信签名对比进行验证
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="timestamp"></param>
        /// <param name="nonce"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool CheckSignature(string signature, string timestamp, string nonce, string token)
        {
            return GetSignature(timestamp, nonce, token).Equals(signature);
        }

        /// <summary>
        /// 本地生成签名
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="nonce"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static string GetSignature(string timestamp, string nonce, string token)
        {
            string[] arr = new[] { token, timestamp, nonce }.OrderBy(z => z).ToArray();
            string arrString = string.Join("", arr);
            System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create();
            byte[] sha1Arr = sha1.ComputeHash(Encoding.UTF8.GetBytes(arrString));
            StringBuilder enText = new StringBuilder();
            foreach (var b in sha1Arr)
            {
                enText.AppendFormat("{0:x2}", b);
            }
            return enText.ToString();
        }
    }
}