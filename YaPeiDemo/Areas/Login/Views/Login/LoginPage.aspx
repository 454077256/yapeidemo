﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>登陆</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#Login").click(function () {
                window.location.href = "/Main/Main/MainPage";

            })
        })
    </script>
</head>
<body style="background-color:#F4F6F8;">

    <div class="he2_con" style=" margin-top:5%; margin-bottom:5%; font-size: 1.3em;  text-align:center;">
        雅培XX登录系统</div>
    <div style="background-color: #fff;  padding: 5px 10px; border-bottom: solid 1px #ddd; border-top: solid 1px #ddd;">
        <div class="he5_con">
            <span style="font-size: 1.2em;">模式：</span>
           
            <form style="float: right;" action="">
            <span style="float: right;">会员</span><input  name="mode" type="radio"
                value="0" style="float: right; margin-top: 8px; margin-left: 1.5em;" />
            <span style="float: right;">用户</span><input  name="mode" type="radio"
                checked="checked" value="1" style="float: right; margin-top: 8px;" />
            </form>
        </div>
    </div>
    <div class="he2_con" style="padding-left: 10px; font-size: 1.2em;">
        登陆信息</div>
    <div style="border-top: solid 1px #ddd;">
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">账号：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input  type="text" value="yapei8888"
                    placeholder="请填写账号(必填)" style="border: none; width: 60%; height: 2.5em; line-height: 2.5em;
                    font-size: 1em; float: right;" />
            </div>
        </div>
        <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
           <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">密码：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input id="Password1" type="password" value="yapei8888"
                    placeholder="请填写密码(必填)" style="border: none; width: 60%; height: 2.5em; line-height: 2.5em;
                    font-size: 1em; float: right;" />
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd; display:none;" >
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">确认密码：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input id="Password2" type="password" value="yapei8888"
                    placeholder="请确认密码(必填)" style="border: none; width: 60%; height: 2em; line-height: 2em;
                    font-size: 1em; float: right;"  />
            </div>
        </div>
      
    </div>
    
   
    <div style=" padding-top: 2em; padding-bottom:2em;">
        <a id="Login" style="width: 94%; margin-left: 3%; margin-right: 3%; background-color: #40affc;
            color: #fff; text-align: center; display: block; font-size: 1.2em; border-radius: 5px;"
            class="he5_con">登 陆</a>
       
    </div>
    <div style=" padding-top: 5em; text-align:center;">
        <a style=" font-size:1.2em; color:#40affc; border-bottom:solid 1px #7cd28f">注 册</a>
    </div>
</body>
</html>
