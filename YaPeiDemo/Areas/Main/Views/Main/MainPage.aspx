﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>操作选项</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#Sell").click(function () {
                window.location.href = "/Market/Sell/SellPage";

            })
            $("#Exam").click(function () {
                window.location.href = "/Market/Exam/ExamPage";

            })
            $("#Notice").click(function () {
                window.location.href = "/Market/Notice/NoticePage";

            })
            $("#Info").click(function () {
                window.location.href = "/Market/Info/InfoPage";

            })
        })
    </script>
</head>
<body style="background-color: #F4F6F8;">
    <div style=" text-align:center;">
        <div style="margin-top: 10%;">
            <img width="50%" src="../../../../Content/images/logo.png" />
        </div>
    </div>
    <div class="he2_con" style="font-size: 1.3em; text-align: center; margin-top:1em; ">
        雅培中国
    </div>
    <div style="background-color: #fff; margin-top: 10%;">
        <table width="100%" cellspacing="0" rules="all" cellpadding="0" style="text-align: center;
            border: solid 1px #eee;">
            <tr>
                <td  id="Sell" style="width: 50%; height: 8em;">
                    <div style="margin-top: 1em;">
                        <img width="30%" src="../../../../Content/images/ico_home.png" />
                    </div>
                    <div style="margin-bottom: 1em; margin-top: 1em; font-weight:lighter; ">
                        数据录入</div>
                </td>
                <td  id="Exam" style="width: 50%; height: 8em;">
                    <div style="margin-top: 1em;">
                        <img width="30%" src="../../../../Content/images/ico_exam.png" />
                    </div>
                    <div style="margin-bottom: 1em; margin-top: 1em; font-weight:lighter;">
                        在线考试</div>
                </td>
            </tr>
            <tr>
                <td  id="Notice" style="width: 50%; height: 8em;">
                    <div style="margin-top: 1em;">
                        <img width="30%" src="../../../../Content/images/ico_notice.png" />
                    </div>
                    <div style="margin-bottom: 1em; margin-top: 1em; font-weight:lighter;">
                       系统消息</div>
                </td>
                <td id="Info" style="width: 50%; height: 8em;">
                    <div style="margin-top: 1em;">
                        <img width="30%" src="../../../../Content/images/ico_infor.png" />
                    </div>
                    <div style="margin-bottom: 1em; margin-top: 1em; font-weight:lighter;">
                        详细资料</div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
