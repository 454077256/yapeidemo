﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/xml; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>我的通知</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
           
        })
    </script>
</head>
<body style="background-color: #F4F6F8;">
    
    <div style="overflow: hidden; background-color: #fff; position: fixed; top: 0; width: 100%; ">
        <a id="Unread" class="top_tab" style="float: left; border-bottom: solid 2px #7cd28f;
            color: #7cd28f;">未读通知</a><a id="Read" class="top_tab" style="float: right;">历史通知</a>
    </div>
    <div id="NewMsg" style=" margin-top:4em;">
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
    </div>

     <div id="OldMsg" style=" margin-top:4em; display:none;">
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
        <div class="noticeitem">
            您的店面这个月被评为 “销量冠军”，将获取雅培公司送出的精美奖品，请联系我们的销售人员。我们将会吧奖品通过快递的方式，邮寄到您的手中。
        </div>
    </div>

    
</body>
</html>
