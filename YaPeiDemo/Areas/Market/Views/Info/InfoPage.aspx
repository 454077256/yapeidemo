﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>详细资料</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#Modify").click(function () {
                window.location.href = "/Market/ModifyInfo/ModifyInfoPage";

            })
        })
    </script>
</head>
<body style="background-color: #F4F6F8;">
    <%--    <div class="he2_con" style=" margin-top:5%; margin-bottom:5%; font-size: 1.3em;  text-align:center;">
        资料修改</div>--%>
    <div class="he2_con" style="padding-left: 10px; font-size: 1.2em; font-weight:bold;">
        门店信息</div>
    <div style="border-top: solid 1px #ddd;">
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">编码：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">YP001</span>
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">店名：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">上海浦东金桥店</span>
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">地址：</span><span style="border: none; width: auto; 
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">上海市浦东新区金桥镇金高路2218弄4号</span>
            </div>
        </div>
    </div>

    <div class="he2_con" style="padding-left: 10px; font-size: 1.2em; font-weight:bold;">
        个人信息</div>
    <div style="border-top: solid 1px #ddd;">
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">编号：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">YP001A</span>
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">姓名：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">张三</span>
            </div>
        </div>
        <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">账号：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">yapei8888</span>
            </div>
        </div>
        <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">手机号：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">15888888888</span>
            </div>
        </div>
        <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">性别：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">男</span>
            </div>
        </div>
       <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">地址：</span><span style="border: none; width: auto;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px; ">上海市浦东新区金桥镇金高路2218弄4号</span>
            </div>
        </div>
    </div>

    <div style="padding-top: 2em; padding-bottom: 2em;">
        <a id="Modify" style="width: 94%; margin-left: 3%; margin-right: 3%; background-color: #40affc;
            color: #fff; text-align: center; display: block; font-size: 1.2em; border-radius: 5px;"
            class="he5_con">修改密码</a>
    </div>
</body>
</html>
