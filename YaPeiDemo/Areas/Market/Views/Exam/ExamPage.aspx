﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/xml; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>在线考试</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
</head>
<body style="background-color: #F4F6F8;">
    <div class="he2_con" style="margin-top: 5%; margin-bottom: 5%; font-size: 1.3em;
        text-align: center;">
        雅培二期培训试题
    </div>
    <div class="tipheader">
            1.雅培生化分析仪光路系统采用以下那种分光方式：
        </div>
    <div class="tipbody">
        
        <div class="tipitem">
            <input type="radio" name="t1">
            A.前分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            B.后分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            C.单波长光束无需分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            D.前分光＋后分光
        </div>
    </div>
    <div class="tipheader">
            2.雅培生化分析仪采用什么作为分光原件：
        </div>
    <div class="tipbody">
        
        <div class="tipitem">
            <input type="radio" name="t1">
            A.滤光片
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            B.凹面衍射光栅
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            C.凸面光栅
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            D.无分光原件
        </div>
    </div>
     <div class="tipheader">
            3.雅培生化分析仪光路系统采用以下那种分光方式：
        </div>
    <div class="tipbody">
       
        <div class="tipitem">
            <input type="radio" name="t1">
            A.前分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            B.后分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            C.单波长光束无需分光
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            D.前分光＋后分光
        </div>
    </div>
     <div class="tipheader">
            4.雅培生化分析仪采用什么作为分光原件：
     </div>
    <div class="tipbody">
       
        <div class="tipitem">
            <input type="radio" name="t1">
            A.滤光片
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            B.凹面衍射光栅
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            C.凸面光栅
        </div>
        <div class="tipitem">
            <input type="radio" name="t1">
            D.无分光原件
        </div>
    </div>
    
    <div style="padding-top: 2em; padding-bottom: 2em;">
        <a id="Submit" style="width: 94%; margin-left: 3%; margin-right: 3%; background-color: #40affc;
            color: #fff; text-align: center; display: block; font-size: 1.2em; border-radius: 5px;"
            class="he5_con">提 交</a>
    </div>
</body>
</html>
