﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>修改密码</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#Submit").click(function () {
                
            })
        })
    </script>
</head>
<body style="background-color: #F4F6F8;">
    <div class="he2_con" style="padding-left: 10px; font-size: 1.2em; font-weight:bold;">
        账户信息</div>
    <div style="border-top: solid 1px #ddd;">
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">账号：</span><span style="border: none; width: auto; height: 2.5em;
                        line-height: 2.5em; font-size: 1em; float: right; margin-right: 10px;">yapei8888</span>
            </div>
        </div>
        <div  style="background-color: #fff; border-bottom: solid 1px #ddd;">
           <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">旧密码：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input id="Password1" type="password" 
                    placeholder="请填写旧密码(必填)" style="border: none; width: 60%; height: 2.5em; line-height: 2.5em;
                    font-size: 1em; float: right;" />
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
           <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">新密码：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input id="Password2" type="password" 
                    placeholder="请填写新密码(必填)" style="border: none; width: 60%; height: 2.5em; line-height: 2.5em;
                    font-size: 1em; float: right;" />
            </div>
        </div>
        <div style="background-color: #fff; border-bottom: solid 1px #ddd;" >
            <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
                z-index: 0; background-color: #fff;">
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">确认密码：</span><i class="icono-caretRight" style="float: right;
                        margin: 5px 10px 0 0px;"></i><input id="Password3" type="password" 
                    placeholder="请确认新密码(必填)" style="border: none; width: 60%; height: 2.5em; line-height: 2.5em;
                    font-size: 1em; float: right;"  />
            </div>
        </div>
      
    </div>
    
   
    <div style=" padding-top: 2em; padding-bottom:2em;">
        <a id="Submit" style="width: 94%; margin-left: 3%; margin-right: 3%; background-color: #40affc;
            color: #fff; text-align: center; display: block; font-size: 1.2em; border-radius: 5px;"
            class="he5_con">提交修改</a>
       
    </div>
</body>
</html>
