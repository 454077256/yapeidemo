﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=no" />
    <title>数据录入</title>
    <link href="../../../../Content/weui.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Content/common.css" type="text/css" rel="stylesheet" />
    <link href="../../../../Content/hoverkicon.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="../../../../Scripts/Common/jquery-1.12.0.min.js"></script>
</head>
<body style="background-color: #F4F6F8;">
    <div class="he2_con" style="margin-top: 5%; margin-bottom: 5%; font-size: 1.3em;
        text-align: center;">
        今日库存管理</div>
    <div style="background-color: #fff; border-bottom: solid 1px #ddd;">
        <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
            border-top: solid 1px #ddd; z-index: 0; background-color: #fff;">
            <div>
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">库存量：</span>
            </div>
            <div>
                <i class="icono-caretRight" style="float: right; margin: 5px 10px 0 0px;"></i>
                <input  type="number" placeholder="请填写库存" style="border: none; width: 60%;
                    height: 2.5em; line-height: 2.5em; font-size: 1em; float: right;" />
            </div>
        </div>
        <div class="he5_con" style="padding-top: 5px; padding-bottom: 5px; position: relative;
            border-top: solid 1px #ddd; z-index: 0; background-color: #fff;">
            <div>
                <span style="font-size: 1.2em; display: inline-block; background-color: #fff; float: left;
                    padding-left: 10px;">销售量：</span></div>
            <div>
                <i class="icono-caretRight" style="float: right; margin: 5px 10px 0 0px;"></i>
                <input type="number" placeholder="请填写今日销售量" style="border: none; width: 60%; height: 2.5em;
                    line-height: 2.5em; font-size: 1em; float: right;" /></div>
        </div>
    </div>
    <div style="padding-top: 2em; padding-bottom: 2em;">
        <a id="Login" style="width: 94%; margin-left: 3%; margin-right: 3%; background-color: #40affc;
            color: #fff; text-align: center; display: block; font-size: 1.2em; border-radius: 5px;"
            class="he5_con">提 交</a>
    </div>
</body>
</html>
